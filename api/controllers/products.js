const knex = require('../config/config')
const Product = require('../models/product')(knex)
const path = require('path')
const jimp = require('jimp')
const fs = require('fs')
const uuid = require('uuid/v4')

const createProduct = (req, res, next) => {
  const props = req.body
  // Optimization and image saving process.
  const photosArray = props.photos.map((photo, index) => {
    const { base64 } = photo
    const base64Data = base64.replace(/^data:image\/(jpeg|jpg|png);base64,/, '')
    const buffer = Buffer.from(base64Data, 'base64')
    const imageId = uuid()

    jimp.read(buffer, (err, image) => {
      if (err) throw err
      image
        .resize(1200, jimp.AUTO)
        .quality(60)
        .write(path.join(__dirname, `../images/${imageId}_${index}.jpg`))
    })
    return `/api/images/${imageId}_${index}.jpg`
  })

  delete props.showModal
  props.photos = photosArray

  Product.create(props)
    .then(product => {
      return res.json({
        ok: true,
        message: 'The product is created',
        product
      })
    })
    .catch(next)
}

const getProduct = (req, res, next) => {
  const id = req.params.product_id
  Product.findById(id)
    .then(product => {
      return res.json({
        ok: true,
        message: 'The product is found',
        product
      })
    })
    .catch(next)
}

const getProducts = (req, res, next) => {
  Product.findAll()
    .then(products => {
      return res.json({
        ok: true,
        message: 'All products are found',
        products
      })
    })
    .catch(next)
}

const getProductsByCategory = (req, res, next) => {
  const category = req.params.category_name
  
  Product.find({
    where: {
      category: category
    }
  }).then(products => {
    return res.json({
      ok: true,
      message: `All products from ${category} are fetched`,
      products
    })
    .catch(next)
  })
}

const updateProduct = (req, res, next) => {
  const id = req.params.product_id
  const props = req.body

  delete props.showModal

  Product.findById(id)
    .then(product => {
      const oldPhotos = product[0].photos
      const photoUrls = props.photos.filter(photo => photo && photo.url).map(photo => '/api' + photo.url)
      const photosToRemove = oldPhotos.filter(photo => !photoUrls.includes(photo))
      
      // Let's remove images if we have such
      if (photosToRemove.length > 0) {
        photosToRemove.forEach(photoPath => {
          fs.unlinkSync(path.join(__dirname, '../' + photoPath.replace('/api', '')))
        })
      }
      
      const photosArray = props.photos.filter(photo => !photo.hasOwnProperty('url')).map((photo, index) => {
        const { base64 } = photo
        const base64Data = base64.replace(/^data:image\/(jpeg|jpg|png);base64,/, '')
        const buffer = Buffer.from(base64Data, 'base64')
        const imageId = uuid()

        jimp.read(buffer, (err, image) => {
          if (err) throw err
          image
            .resize(1200, jimp.AUTO)
            .quality(60)
            .write(path.join(__dirname, `../images/${imageId}_${index}.jpg`))
        })
        return `/api/images/${imageId}_${index}.jpg`
      })

      props.photos = photoUrls.concat(photosArray)

      Product.update(id, { ...props })
        .then(product => {
          return res.json({
            ok: true,
            message: 'The product is updated',
            product
          })
        })
        .catch(next)
    })
    .catch(next)
}

const deleteProduct = (req, res, next) => {
  const id = req.params.product_id

  Product.destroy(id)
    .then(deleteCount => {
      return res.json({
        ok: true,
        message: 'The product is removed',
        deleteCount
      })
    })
    .catch(next)
}

module.exports = {
  createProduct,
  getProduct,
  getProducts,
  updateProduct,
  deleteProduct, 
  getProductsByCategory
}