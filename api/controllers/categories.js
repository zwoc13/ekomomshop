const knex = require('../config/config')
const Category = require('../models/category')(knex)

const getCategories = (req, res, next) => {
  Category.findAll()
    .then(category => {
      return res.json({
        ok: true,
        message: 'Fetched all categories',
        categories: category
      })
    })
    .catch(next)
}

const createCategory = (req, res, next) => {
  const { name, description } = req.body

  return Category.create({ name, description })
    .then(category => {
      return res.json({
        ok: true,
        message: 'Category created',
        category: category[0]
      })
    })
    .catch(next)
}

const updateCategory = (req, res, next) => {
  const id = req.params.category_id
  const { name, description } = req.body

  return Category.update(id, { name, description })
    .then(category => {
      return res.json({
        ok: true,
        message: 'Category updated',
        category
      })
    })
    .catch(next)
}

const deleteCategory = (req, res, next) => {
  const id = req.params.category_id

  return Category.destroy(id)
    .then(count => {
      return res.json({
        ok: true,
        message: 'Category removed',
        deleteCount: count
      })
    })
    .catch(next)
}

module.exports = {
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory,
}