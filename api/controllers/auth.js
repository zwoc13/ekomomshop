const knex = require('../config/config')
const User = require('../models/user')(knex)
const jwt = require('jsonwebtoken')
const { JWT_SECRET } = require('../config/global')

const me = (req, res) => {
  return res.json({
    ok: true,
    user: req.user
  })  
}

const login = (req, res, next) => {
  const { email, password } = req.body

  User.verify(email, password)
    .then(user => {
      const { name, phone, email, role } = user
      const token = jwt.sign({ name, phone, email, role }, JWT_SECRET, { expiresIn: '2d' })
      const responseUser = {
        name,
        phone,
        email,
        role,
        token
      }
      return next(res.json({
        message: 'Logged in',
        user: responseUser
      }))
    })
    .catch(err => {
      return res.status(404).json({ err, message: 'This user doesn\'t exist inside database' })
    })
}

const logout = (req, res, next) => {
  req.logout()
  res.json({ ok: true })
}

const register = (req, res, next) => {
  const props = req.body

  User.findOne({ email: props.email })
    .then(user => {
      if (user) {
        return res.json({
          status: 400,
          message: 'The user is already registered'
        })
      }

      return User.create({ ...props })
        .then(() => {
          return res.json(user)
        })
        .catch(err => {
          return err.status(500).send('Error during creation of the user')
        })
    })
    .then(user => res.json({
      ok: true,
      message: 'Registered',
      user
    }))
    .catch(next)
}

module.exports = {
  me,
  login,
  logout,
  register
}