const knex = require('../config/config')
const Tissue = require('../models/tissue')(knex)

const getTissues = (req, res, next) => {
  Tissue.findAll()
    .then(tissues => {
      return res.json({
        ok: true,
        tissues
      })
    })
    .catch(next)
}

const createTissue = (req, res, next) => {
  const { name } = req.body

  Tissue.create({ name })
    .then(tissue => {
      return res.json({
        ok: true,
        tissue
      })
    })
    .catch(next)
}

const updateTissue = (req, res, next) => {
  const id = req.params.tissue_id
  const { name } = req.body

  Tissue.update(id, { name })
    .then(tissue => {
      return res.json({
        ok: true,
        tissue
      })
    })
    .catch(next)
}

const deleteTissue = (req, res, next) => {
  const id = req.params.tissue_id

  Tissue.destroy(id)
    .then(count => {
      return res.json({
        ok: true,
        count
      })
    })
    .catch(next)
}

module.exports = {
  getTissues,
  createTissue,
  updateTissue,
  deleteTissue
}