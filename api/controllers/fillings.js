const knex = require('../config/config')
const Filling = require('../models/fillings')(knex)

const getFillings = (req, res, next) => {
  Filling.findAll()
    .then(fillings => {
      return res.json({
        ok: true,
        fillings
      })
    })
    .catch(next)
}

const createFilling = (req, res, next) => {
  const { name } = req.body

  Filling.create({ name })
    .then(filling => {
      return res.json({
        ok: true,
        filling
      })
    })
    .catch(next)
}

const updateFilling = (req, res, next) => {
  const id = req.params.filling_id
  const { name } = req.body

  Filling.update(id, { name })
    .then(filling => {
      return res.json({
        ok: true,
        filling
      })
    })
    .catch(next)
}

const deleteFilling = (req, res, next) => {
  const id = req.params.filling_id

  Filling.destroy(id)
    .then(count => {
      return res.json({
        ok: true,
        count
      })
    })
    .catch(next)
}

module.exports = {
  getFillings,
  createFilling,
  updateFilling,
  deleteFilling
}