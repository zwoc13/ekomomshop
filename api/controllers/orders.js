const knex = require('../config/config')
const Order = require('../models/order')(knex)

const createOrder = (req, res, next) => {
  const props = req.body

  Order.create({ ...props })
    .then(order => {
      return res.json({
        ok: true,
        message: 'Order created',
        order
      })
    })
    .catch(next)
}

const getOrders = (req, res, next) => {
  Order.findAll()
    .then(orders => {
      return res.json({
        ok: true,
        message: 'Orders found',
        orders
      })
    })
    .catch(next)
}

const getOrder = (req, res, next) => {
  const id = req.params.order_id

  Order.findById(id)
    .then(order => {
      return res.json({
        ok: true,
        message: 'Order found',
        order
      })
    })
    .catch(next)
}

const updateOrder = (req, res, next) => {
  const id = req.params.order_id
  const props = req.body

  Order.update(id, { ...props })
    .then(order => {
      return res.json({
        ok: true,
        message: 'Order updated',
        order
      })
    })
    .catch(next)
}

const deleteOrder = (req, res, next) => {
  const id = req.params.order_id

  Order.destroy(id)
    .then(deleteCount => {
      return res.json({
        ok: true,
        message: 'Order destroyed',
        deleteCount
      })
    })
    .catch(next)
}

module.exports = {
  createOrder,
  getOrder,
  getOrders,
  updateOrder,
  deleteOrder
}