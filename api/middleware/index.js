const jwt = require('jsonwebtoken')
const { JWT_SECRET } = require('../config/global')

const verifyToken = (req, res, next) => { 
  if (req.hasOwnProperty('headers') && req.headers.hasOwnProperty('authorization')) {
    try {
      const authToken = req.headers.authorization.replace('Bearer ', '')
      const user = jwt.verify(authToken, JWT_SECRET)
      req.user = user
      next()
    } catch(err) {
      return next()
    }
  } else {
    return next('No auth token presets inside req.headers')
  }
}

module.exports = {
  verifyToken
}