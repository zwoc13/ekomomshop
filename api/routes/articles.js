const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  createArticle,
  getArticles,
  getArticle,
  updateArticle,
  deleteArticle
} = require('../controllers/articles')

router.post('/articles', verifyToken, createArticle)
router.get('/articles', getArticles)

router.get('/articles/:article_id', getArticle)
router.put('/articles/:article_id', verifyToken, updateArticle)
router.delete('/articles/:article_id', verifyToken, deleteArticle)

module.exports = router