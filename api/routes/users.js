const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  createUser,
  getUser,
  getUsers,
  updateUser,
  deleteUser,
  deleteUserByName
} = require('../controllers/users')

router.post('/users', createUser)
router.get('/users', verifyToken, getUsers)

router.get('/users/:user_id', verifyToken, getUser)
router.put('/users/:user_id', verifyToken, updateUser)
router.delete('/users/:user_id', verifyToken, deleteUser)
router.delete('/users/name/:name', verifyToken, deleteUserByName)

module.exports = router