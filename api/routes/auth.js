const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  me,
  login,
  logout,
  register
} = require('../controllers/auth')

router.post('/me', verifyToken, me)
router.post('/login', login)
router.post('/logout', logout)
router.post('/register', register)

module.exports = router