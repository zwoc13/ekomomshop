const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  createProduct,
  getProduct,
  getProducts,
  updateProduct,
  deleteProduct, 
  getProductsByCategory
} = require('../controllers/products')

router.get('/category/:category_name', getProductsByCategory)

router.post('/products', verifyToken, createProduct)
router.get('/products', getProducts)

router.post('/products/comment', updateProduct)

router.post('/products/rating', updateProduct)

router.get('/products/:product_id', getProduct)
router.put('/products/:product_id', verifyToken, updateProduct)
router.delete('/products/:product_id', verifyToken, deleteProduct)

module.exports = router