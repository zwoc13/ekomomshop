const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  getFillings,
  createFilling,
  updateFilling,
  deleteFilling
} = require('../controllers/fillings')

router.get('/fillings', verifyToken, getFillings)
router.post('/fillings', verifyToken, createFilling) 
router.put('/fillings/:filling_id', verifyToken, updateFilling)
router.delete('/fillings/:filling_id', verifyToken, deleteFilling)

module.exports = router