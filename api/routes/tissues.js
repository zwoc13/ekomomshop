const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  getTissues,
  createTissue,
  updateTissue,
  deleteTissue
} = require('../controllers/tissues')

router.get('/tissues', verifyToken, getTissues)
router.post('/tissues', verifyToken, createTissue)
router.put('/tissues/:tissue_id', verifyToken, updateTissue)
router.delete('/tissues/:tissue_id', verifyToken, deleteTissue)

module.exports = router