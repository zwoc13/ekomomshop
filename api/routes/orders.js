const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  createOrder,
  getOrder,
  getOrders,
  updateOrder,
  deleteOrder
} = require('../controllers/orders')

router.post('/orders', verifyToken, createOrder)
router.get('/orders', verifyToken, getOrders)

router.get('/orders/:order_id', getOrder)
router.put('/orders/:order_id', verifyToken, updateOrder)
router.delete('/orders/:order_id', verifyToken, deleteOrder)

module.exports = router