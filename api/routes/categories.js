const router = require('express').Router()
const { verifyToken } = require('../middleware')
const {
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory,
} = require('../controllers/categories')

router.get('/categories', getCategories)
router.post('/categories', verifyToken, createCategory)
router.put('/categories/:category_id', verifyToken, updateCategory)
router.delete('/categories/:category_id', verifyToken, deleteCategory)

module.exports = router