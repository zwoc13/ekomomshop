const createGuts = require('../helpers/model-guts')
const name = 'Article'
const tableName = 'articles'


const selectableProps = [
  'id',
  'visible',
  'header',
  'content',
  'created_at',
  'updated_at'
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName, 
    selectableProps
  })

  return {
    ...guts
  }
}