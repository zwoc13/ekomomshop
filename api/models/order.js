const createGuts = require('../helpers/model-guts')
const name = 'Order'
const tableName = 'orders'

const selectableProps = [
  'id',
  'products',
  'customer',
  'shipping_address',
  'comment',
  'status',
  'created_at',
  'updated_at',
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  return {
    ...guts
  }
}