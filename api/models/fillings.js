const createGuts = require('../helpers/model-guts')
const name = 'Filling'
const tableName = 'fillings'

const selectableProps = [
  'id',
  'name'
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  return {
    ...guts
  }
}