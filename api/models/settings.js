const createGuts = require('../helpers/model-guts')
const name = 'Settings'
const tableName = 'settings'

const selectableProps = [
  'id',
  'sliders',
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  return {
    ...guts
  }
}