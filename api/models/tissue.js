const createGuts = require('../helpers/model-guts')
const name = 'Tissue'
const tableName = 'tissues'

const selectableProps = [
  'id',
  'name'
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  return {
    ...guts
  }
}