const bcrypt = require('bcrypt')
const createGuts = require('../helpers/model-guts')
const name = 'User'
const tableName = 'users'
const timeout = 1000

const selectableProps = [
  'id',
  'name',
  'email',
  'password',
  'orders',
  'phone',
  'role'
]

const SALT_ROUNDS = 10
const hashPassword = password => bcrypt.hash(password, SALT_ROUNDS)
const verifyPassword = (password, hash) => bcrypt.compare(password, hash)

const beforeSave = user => {
  if (!user.password) return Promise.resolve(user)

  return hashPassword(user.password)
    .then(hash => ({ ...user, password: hash }))
    .catch(err => `Error hashing password: ${err}`)
}

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  const create = props => beforeSave(props)
    .then(user => guts.create(user))

  const verify = (email, password) => {
    const matchErrorMsg = 'Email or password are incorrect'

    return knex.select()
      .from(tableName)
      .where({ email })
      .then(usersList => {
        if (!usersList) throw matchErrorMsg
        return usersList[0]
      })
      .then(user => Promise.all([ user, verifyPassword(password, user.password) ]))
      .then(([ user, isMatch ]) => {
        if (!isMatch) throw matchErrorMsg
        return user
      })
  }

  return {
    ...guts,
    create,
    verify,
  }
}