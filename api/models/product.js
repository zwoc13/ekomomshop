const createGuts = require('../helpers/model-guts')
const name = 'Product'
const tableName = 'products'

const selectableProps = [
  'id',
  'name',
  'price',
  'description',
  'visible', 
  'tissue',
  'filling',
  'photos',
  'qnt',
  'onSales',
  'salesPrice',
  'category',
  'comments',
  'rating',
  'created_at',
  'updated_at'
]

module.exports = knex => {
  const guts = createGuts({
    knex,
    name,
    tableName,
    selectableProps
  })

  return {
    ...guts
  }
}