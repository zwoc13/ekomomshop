module.exports = ({
  knex = {},
  name = 'name',
  tableName = 'tableName',
  selectableProps = [],
  timeout = 1000
}) => {
  const create = props => {
    delete props.id

    return knex.insert(props)
      .returning(selectableProps)
      .into(tableName)
  }

  const findAll = () => (
    knex.select(selectableProps)
      .from(tableName)
      .orderBy('id', 'asc')
  )

  const find = filters => (
    knex.select(selectableProps)
      .from(tableName)
      .where(filters)
      .orderBy('id', 'asc')
  )

  const findOne = filters => (
    find(filters)
      .then(results => {
        if (!Array.isArray(results)) return results
        return results[0]
      })
  )

  const findById = id => (
    knex.select(selectableProps)
      .from(tableName)
      .where({ id })
  )

  const update = (id, props) => {
    delete props.id

    return knex.update(props)
      .from(tableName)
      .where({ id })
      .returning(selectableProps)
      .orderBy('id', 'asc')
  }

  const updateByField = (id, field, value) => {
    delete props.id

    return knex.from(tableName)
      .where({ id })
      .update(field, value)
      .returning(selectableProps)
      .orderBy('id', 'asc')
  }

  const destroy = id => (
    knex.del()
      .from(tableName)
      .where({ id })
  )

  const destroyByField = (field, value) => {
    return knex.del()
      .from(tableName)
      .where(field, value)
  }

  return {
    name,
    tableName,
    selectableProps,
    timeout,
    create,
    findAll,
    find,
    findOne,
    findById,
    update,
    updateByField,
    destroy,
    destroyByField
  }
}