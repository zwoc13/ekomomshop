
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('articles', t => {
      t.increments('id').unsigned().primary()
      t.boolean('visible')
      t.string('header')
      t.string('content')
      t.timestamp('created_at').defaultTo(knex.fn.now())
      t.timestamp('updated_at').defaultTo(knex.fn.now())
    }),

    knex.schema.createTable('categories', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
      t.string('description')
    }),

    knex.schema.createTable('tissues', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
    }),

    knex.schema.createTable('fillings', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
    }),

    knex.schema.createTable('products', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
      t.float('price')
      t.string('description')
      t.boolean('visible')
      t.string('tissue').references('tissues')
      t.string('filling').references('fillings')
      t.specificType('photos', 'text ARRAY')
      t.integer('qnt')
      t.boolean('onSales')
      t.float('salesPrice')
      t.string('category').references('categories')
      t.specificType('comments', 'jsonb[]')
      t.float('rating')
      t.timestamp('created_at').defaultTo(knex.fn.now())
      t.timestamp('updated_at').defaultTo(knex.fn.now())
    }),

    knex.schema.createTable('users', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
      t.string('email')
      t.string('phone')
      t.string('password')
      t.specificType('orders', 'jsonb[]').references('orders')
      t.string('role').defaultTo('user')
      t.timestamp('created_at').defaultTo(knex.fn.now())
      t.timestamp('updated_at').defaultTo(knex.fn.now())
    }),

    knex.schema.createTable('orders', t => {
      t.increments('id').unsigned().primary()
      t.specificType('products', 'jsonb[]')
      t.jsonb('customer').references('users')
      t.jsonb('shipping_address')
      t.string('comment')
      t.string('status')
      t.string('ttn')
      t.timestamp('created_at').defaultTo(knex.fn.now())
      t.timestamp('updated_at').defaultTo(knex.fn.now())
    }),

    knex.schema.createTable('blacklist', t => {
      t.increments('id').unsigned().primary()
      t.string('name')
      t.string('phone')
      t.string('comment')
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('articles'),
    knex.schema.dropTable('products'),
    knex.schema.dropTable('users'),
    knex.schema.dropTable('orders'),
    knex.schema.dropTable('categories'),
    knex.schema.dropTable('tissues'),
    knex.schema.dropTable('fillings'),
    knex.schema.dropTable('blacklist')
  ])
};
