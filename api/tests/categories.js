process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

const { expect } = chai
chai.use(chaiHttp)

const fs = require('fs')
const path = require('path')
const fetchToken = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, './token.txt'), 'utf-8', (err, authToken) => {
      return err ? reject(err) : resolve(authToken)
    })
  })
}

describe('Categories controllers', () => {
  let token

  before(async () => {
    return await fetchToken()
      .then(authToken => token = authToken)
      .catch(err => console.error(err))
  })

  it('Should create new category', (done) => {
    const body = {
      props: {
        name: 'kitty',
        description: 'The kitty is a really cute creature'
      }
    }
    chai.request(server)
      .post('/categories')
      .send(body)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('Category created')
        expect(res.body.category).to.have.keys(['id', 'name', 'description'])
        done()
      })
  })

  it('Should fetch all categories', (done) => {
    chai.request(server)
      .get('/categories/')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('Fetched all categories')
        expect(res.body.categories.length).to.be.gt(0)
        done()
      })
  })

  it('Should update the category name to kittens', (done) => {
    const body = {
      props: {
        name: 'kittens',
        description: 'Kittens are the most famous and cutiest creatures in our lives'
      }
    }
    chai.request(server)
      .put('/categories/name/kitty')
      .set('Authorization', `Bearer ${token}`)
      .send(body)
      .end((err, res) => {
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('Category updated')
        expect(res.body.category).to.have.keys(['id', 'name', 'description'])
        done() 
      })
  })

  it('Should remove the category', (done) => {
    chai.request(server)
      .del('/categories/name/kittens')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('Category removed')
        expect(res.body.deleteCount).to.equal(1)
        done()
      })
  })

})