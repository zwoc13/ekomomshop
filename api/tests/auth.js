process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

const { expect } = chai
chai.use(chaiHttp)

const fs = require('fs')
const path = require('path')
const fetchToken = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, './token.txt'), 'utf-8', (err, authToken) => {
      return err ? reject(err) : resolve(authToken)
    })
  })
}

describe('Auth controllers', () => {
  let token

  before(async () => {
    return await fetchToken()
      .then(authToken => token = authToken)
      .catch(err => console.error(err))
  })

  it('Should remove an old user', (done) => {
    chai.request(server)
      .del('/users/name/Test User')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res.status).to.equal(200)
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('User destroyed')
        expect(res.body.count).to.equal(1)
        done()
      })
  })

  it('Should register dummy user', (done) => {
    const props = {
      user: {
        name: 'Test User',
        email: 'test@test.com',
        password: 'demoPassword',
        phone: '380808080',
        role: 'admin'
      }
    }
    chai.request(server)
      .post('/register')
      .send(props)
      .end((err, res) => {
        expect(res.body.ok).be.true
        expect(res.body.message).to.equal('Registered')
        expect(res.status).to.equal(200)
        done()
      })
  })

  it('Should login and get the auth token', (done) => {
    const props = {
      email: 'test@test.com',
      password: 'demoPassword'
    }
    chai.request(server)
      .post('/login')
      .send(props)
      .end((err, res) => {
        const newToken = res.body.user.token
        expect(res.body.ok).to.be.true
        expect(res.status).to.equal(200)
        expect(res.body.message).to.equal('Logged in')
        expect(newToken).to.be.a('string')

        // Let's write the new token to the token file
        fs.writeFile(path.join(__dirname, './token.txt'), newToken, (err) => {
          if (err) console.log(err)
        })

        done()
      })
  })

})