const express = require('express')
const bodyParser =  require('body-parser')
const { Nuxt, Builder } = require('nuxt')
const path = require('path')

const app = express()
const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3010

app.set('port', port)
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }))
// Static images serving
app.use('/api/images', express.static(path.join(__dirname, './images')))

// Routing
app.use('/api', [
  require('./routes/articles'),
  require('./routes/products'),
  require('./routes/users'),
  require('./routes/orders'),
  require('./routes/auth'),
  require('./routes/fillings'),
  require('./routes/tissues'),
  require('./routes/categories')
])

app.get('/api', (req, res) => res.json({ message: "Welcome to Eko_Mom shop API", ok: true }))

let config = require('../nuxt.config')
config.dev = !(process.env.NODE_ENV === 'production')

const nuxt = new Nuxt(config)

if (config.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

app.use(nuxt.render)
app.listen(port, host)