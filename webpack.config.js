module.exports = {
  module: {
    rules: [
      {
        test: '/\.js$/',
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        text: '/\.vue$/',
        exclude: /node_modules/,
        loader: 'vue-loader'
      },
      {
        test: '/\.less$/',
        use: [
          'vue-style-loader',
          'css-loader',
          'less-loader'
        ]
      }
    ]
  }
}