module.exports = {
  css: [
    '@/assets/main.css'
  ],
  head: {
    title: 'EkoMom - Інтернет-магазин для дітей та баткьів',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Інтернет-магазин для дітей та батьків' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Oswald:300,400|Roboto&display=swap&subset=cyrillic' }
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast',
    ['nuxt-vue-material', {
      theme: null
    }]
  ],
  axios: {
    prefix: '/api',
    host: 'localhost',
    port: 3010,
  },
  plugins: [
    { src: '@/plugins/carousel.js', ssr: false },
  ],
  loading: '@/components/Loading.vue',
  auth: {
    localStorage: false,
    plugins: [
      '@/plugins/auth.js'
    ],
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'user.token' },
          user: { url: '/me', method: 'post', propertyName: 'user' }
        }
      }
    }
  },
  toast: {
    position: 'top-center',
    theme: 'outline',
    duration: 1200
  },
}

